using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Runtime.CompilerServices;

public class WeightChanger : MonoBehaviour
{
    public TextMeshProUGUI textOfWeight;
    public GameObject player;
    public float currentMass;
    private bool playerOnScales = false;
    private bool oneTime = true;
    public float timeMass = 0;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    private void OnCollisionEnter(Collision collision)
    {  
        currentMass += collision.gameObject.GetComponent<Rigidbody>().mass;
    }
    private void OnCollisionExit(Collision collision)
    {
        currentMass -= collision.gameObject.GetComponent<Rigidbody>().mass;
    }
    private void OnTriggerEnter(Collider other)
    {
        currentMass += other.GetComponent<Rigidbody>().mass;
        if (other.gameObject.tag == "Player")
        {
            playerOnScales = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        currentMass -= other.GetComponent<Rigidbody>().mass;
        if (other.gameObject.tag == "Player")
        {
            playerOnScales = false;
        }
    }
    private void Update()
    {
        textOfWeight.text = currentMass.ToString() + " ��";
        if (playerOnScales)
        {
            if (player.GetComponent<InteractionManager>().CurrentSelectable && oneTime)
            {
                currentMass += player.GetComponent<InteractionManager>().CurrentSelectable.GetComponent<Rigidbody>().mass;
                timeMass = player.GetComponent<InteractionManager>().CurrentSelectable.GetComponent<Rigidbody>().mass;
                oneTime = false;
            }
            else if (!player.GetComponent<InteractionManager>().CurrentSelectable)
            {
                currentMass -= timeMass;
                timeMass = 0;
                oneTime = true;
            }
        }
        else
        {
            currentMass -= timeMass;
            timeMass = 0;
            oneTime = true;
        }
    }
}