using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnEye : MonoBehaviour
{
    private GameObject player;
    public Vector3 startPos;
    public Vector3 endPos;
    public GameObject line;
    public GameObject canvasLine;
    public GameObject objectLine;
    public GameObject objectCanvas = null;
    public bool objectsActive = false;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    public void Select()
    {
        gameObject.transform.parent = player.transform.GetChild(0);
        gameObject.transform.localPosition = new Vector3(0, -0.2f, 1);
        gameObject.transform.localRotation = new Quaternion(0, 0, 0, 0);
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
    }
    public void Deselect()
    {
        gameObject.transform.parent = null;
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        startPos = gameObject.transform.position;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (gameObject.GetComponent<TrailRenderer>().emitting && collision.gameObject.tag != "Player")
        {
            gameObject.GetComponent<TrailRenderer>().emitting = false;
            endPos = gameObject.transform.position;
            line.GetComponent<LineRenderer>().SetPosition(0, startPos);
            line.GetComponent<LineRenderer>().SetPosition(1, endPos);
            canvasLine.GetComponentInChildren<TextMeshProUGUI>().text = (endPos - startPos).magnitude.ToString() + " �";
            objectLine = Instantiate(line);
            objectCanvas = Instantiate(canvasLine, (startPos + endPos) / 2 + new Vector3(0, 0.5f, 0), player.transform.rotation);
            objectLine.transform.SetParent(gameObject.transform, false); 
            objectCanvas.transform.SetParent(objectLine.transform, true);
            objectsActive = true;
        }
    }
}
