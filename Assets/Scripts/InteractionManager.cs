using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class InteractionManager : MonoBehaviour
{
    public GameObject buttonToDiolog;
    public GameObject npc;
    private GameObject player;
    public GameObject dialogSystem;
    public OnEye CurrentSelectable;
    public AudioSource npcVoice;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("MainCamera");
        Time.timeScale = 1;
        gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.m_cursorIsLocked = true;
        if (SceneManager.GetActiveScene().name != "VirtuaLab")
        {
            buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, 0);
            dialogSystem.SetActive(false);
        } 
        if (SceneManager.GetActiveScene().name == "MoonScene")
        {
            Physics.gravity *= 0.166f;
        }
        if (SceneManager.GetActiveScene().name != "MoonScene")
        {
            Physics.gravity = new Vector3(0, -9.8f, 0);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) && SceneManager.GetActiveScene().name == "VirtuaLab")
        {
            if (GameObject.FindGameObjectsWithTag("Prop").Length > 0)
            {
                Destroy(GameObject.FindGameObjectsWithTag("Prop")[GameObject.FindGameObjectsWithTag("Prop").Length - 1]);
            }
        }
        if (SceneManager.GetActiveScene().name == "BridgePractice")
        {
            if (CurrentSelectable)
            {
                if (CurrentSelectable.name.Split(' ')[1] == "Correct" && dialogSystem.activeSelf)
                {
                    dialogSystem.GetComponent<BridgePractice>().passThePractice = true;
                }
                else if (CurrentSelectable.name.Split(' ')[1] != "Correct" && dialogSystem.activeSelf)
                {
                    dialogSystem.GetComponent<BridgePractice>().dontPassThePractice = true;
                }
                else
                {
                    dialogSystem.GetComponent<BridgePractice>().dontPassThePractice = false;
                    dialogSystem.GetComponent<BridgePractice>().passThePractice = false;
                }
            }
            else
            {
                dialogSystem.GetComponent<BridgePractice>().dontPassThePractice = false;
                dialogSystem.GetComponent<BridgePractice>().passThePractice = false;
            }
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        { 
            if (SceneManager.GetActiveScene().name != "BridgePractice")
            {
                if (hit.distance <= 5 && hit.collider.gameObject.name == "Robot" && player.GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled)
                {
                    npc.transform.rotation = new Quaternion(Mathf.Lerp(npc.transform.rotation.x, player.transform.rotation.x, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.y, player.transform.rotation.y, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.z, player.transform.rotation.z, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.w, player.transform.rotation.w, Time.deltaTime * 2));
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(buttonToDiolog.GetComponent<Image>().color.a, 0, Time.deltaTime * 2));
                        player.GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
                        npcVoice.Play();
                        dialogSystem.SetActive(true);
                        if (SceneManager.GetActiveScene().name == "Hub")
                        {
                            dialogSystem.GetComponent<DialogSystem>().StartDialog();
                        }
                        if (SceneManager.GetActiveScene().name == "earthScene")
                        {
                            dialogSystem.GetComponent<EarthDialogSystem>().StartDialog();
                        }

                    }
                    else if (!dialogSystem.activeSelf)
                    {
                        npc.transform.rotation = new Quaternion(Mathf.Lerp(npc.transform.rotation.x, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.y, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.z, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.w, 0, Time.deltaTime * 2));
                        buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(buttonToDiolog.GetComponent<Image>().color.a, 1, Time.deltaTime * 2));
                        npcVoice.Stop();
                    }
                }
                else 
                {
                    if (SceneManager.GetActiveScene().name != "VirtuaLab")
                    {
                        if (!dialogSystem.activeSelf)
                        {
                            npc.transform.rotation = new Quaternion(Mathf.Lerp(npc.transform.rotation.x, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.y, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.z, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.w, 0, Time.deltaTime * 2));
                            buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(buttonToDiolog.GetComponent<Image>().color.a, 0, Time.deltaTime * 2));
                            npcVoice.Stop();
                        } 
                    }
                }
            }
            else
            {
                if (hit.distance <= 5 && hit.collider.gameObject.tag == "Bot" && player.GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled)
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(buttonToDiolog.GetComponent<Image>().color.a, 0, Time.deltaTime * 2));
                        player.GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
                        npcVoice.Play();
                        dialogSystem.SetActive(true);
                        dialogSystem.GetComponent<BridgePractice>().StartDialog();
                    }
                    else if (!dialogSystem.activeSelf)
                    {
                        buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(buttonToDiolog.GetComponent<Image>().color.a, 1, Time.deltaTime * 2));
                        npcVoice.Stop();
                    }
                }
                else
                {
                    if (SceneManager.GetActiveScene().name != "VirtuaLab")
                    {
                        if (!dialogSystem.activeSelf)
                        {
                            buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(buttonToDiolog.GetComponent<Image>().color.a, 0, Time.deltaTime * 2));
                            npcVoice.Stop();
                        }    
                    }
                }
            }
            
        }
        else
        {
            if (SceneManager.GetActiveScene().name != "VirtuaLab")
            {
                npc.transform.rotation = new Quaternion(Mathf.Lerp(npc.transform.rotation.x, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.y, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.z, 0, Time.deltaTime * 2), Mathf.Lerp(npc.transform.rotation.w, 0, Time.deltaTime * 2));
                buttonToDiolog.GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(buttonToDiolog.GetComponent<Image>().color.a, 0, Time.deltaTime * 2));
            }              
        }

        if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.tag == "Prop")
        {
            OnEye selectable = hit.collider.gameObject.GetComponent<OnEye>();
            if (hit.distance <= 5 && selectable)
            {
                if (CurrentSelectable == selectable && Input.GetKeyDown(KeyCode.R))
                {
                    CurrentSelectable.Deselect();
                    CurrentSelectable = null;
                }
                else if (Input.GetKeyDown(KeyCode.R) && selectable)
                {
                    CurrentSelectable = selectable;
                    selectable.Select();
                    CurrentSelectable.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.None;
                }
                if (CurrentSelectable && Input.GetMouseButton(0))
                {
                    CurrentSelectable.Deselect();
                    CurrentSelectable.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
                    CurrentSelectable.GetComponent<Rigidbody>().AddForce(ray.direction * 2, ForceMode.Impulse);
                    CurrentSelectable.GetComponent<TrailRenderer>().emitting = true;
                    CurrentSelectable = null;
                    if (SceneManager.GetActiveScene().name == "earthScene")
                    {
                        if (dialogSystem.GetComponent<EarthDialogSystem>().earthThisNumDialogIsFinished == 1)
                        {
                            dialogSystem.GetComponent<EarthDialogSystem>().passTheThirdActionTheApple = true;
                        }
                    }
                    if (SceneManager.GetActiveScene().name == "MoonScene")
                    {
                        if (dialogSystem.GetComponent<MoonDialogSystem>().thisMoonNumDialogIsFinished == 1)
                        {
                            dialogSystem.GetComponent<MoonDialogSystem>().passTheSixthActionTheApple = true;
                        }
                    }
                }
            }
            else
            {
                if (CurrentSelectable && Input.GetKeyDown(KeyCode.R))
                {
                    CurrentSelectable.Deselect();
                    CurrentSelectable = null;
                }
                if (CurrentSelectable && Input.GetMouseButton(0))
                {
                    CurrentSelectable.Deselect();
                    CurrentSelectable.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
                    CurrentSelectable.GetComponent<Rigidbody>().AddForce(ray.direction * 2, ForceMode.Impulse);
                    CurrentSelectable.GetComponent<TrailRenderer>().emitting = true;
                    CurrentSelectable = null;
                    if (SceneManager.GetActiveScene().name == "earthScene")
                    {
                        if (dialogSystem.GetComponent<EarthDialogSystem>().earthThisNumDialogIsFinished == 1)
                        {
                            dialogSystem.GetComponent<EarthDialogSystem>().passTheThirdActionTheApple = true;
                        }
                    }
                    if (SceneManager.GetActiveScene().name == "MoonScene")
                    {
                        if (dialogSystem.GetComponent<MoonDialogSystem>().thisMoonNumDialogIsFinished == 1)
                        {
                            dialogSystem.GetComponent<MoonDialogSystem>().passTheSixthActionTheApple = true;
                        }
                    }
                }
            }
        }
        else
        {
            if (CurrentSelectable && Input.GetKeyDown(KeyCode.R))
            {
                CurrentSelectable.Deselect();
                CurrentSelectable = null;
            }
            if (CurrentSelectable && Input.GetMouseButton(0))
            {
                CurrentSelectable.Deselect();
                CurrentSelectable.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
                CurrentSelectable.GetComponent<Rigidbody>().AddForce(ray.direction * 2, ForceMode.Impulse);
                CurrentSelectable.GetComponent<TrailRenderer>().emitting = true;
                CurrentSelectable = null;
                if (SceneManager.GetActiveScene().name == "earthScene")
                {
                    if (dialogSystem.GetComponent<EarthDialogSystem>().earthThisNumDialogIsFinished == 1)
                    {
                        dialogSystem.GetComponent<EarthDialogSystem>().passTheThirdActionTheApple = true;
                    }
                }
                if (SceneManager.GetActiveScene().name == "MoonScene")
                {
                    if (dialogSystem.GetComponent<MoonDialogSystem>().thisMoonNumDialogIsFinished == 1)
                    {
                        dialogSystem.GetComponent<MoonDialogSystem>().passTheSixthActionTheApple = true;
                    }
                }
            }
        }

    }
}
