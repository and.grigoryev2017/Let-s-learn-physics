using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Cinemachine.CinemachineFreeLook;
using Valve.VR.InteractionSystem;

public class PlayerTracker : MonoBehaviour
{
    private GameObject player;
    private Vector3 start;
    private Vector3 end;
    private bool thatsAll = false;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

    }
    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponentInParent<OnEye>().objectsActive && !thatsAll)
        {
            start = gameObject.GetComponentInParent<OnEye>().startPos;
            end = gameObject.GetComponentInParent<OnEye>().endPos;
            thatsAll = true;
        }
        if (gameObject.GetComponentInParent<OnEye>().objectsActive)
        {
            transform.position = (start + end) / 2 + new Vector3(0, 0.5f, 0);
            transform.rotation = player.transform.rotation;
        }
            
    }
}
