using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class FirstLesson : MonoBehaviour
{
    public GameObject dialogScreen;
    private string[] timelyText;
    private int count;
    private FilmGrain filmGrain;
    private ChromaticAberration ca;
    private Vignette vig;
    private float sizeOfEffect1;
    private float sizeOfEffect2;
    private float sizeOfEffect3;
    private bool readyToTeleport = false;
    public GameObject whitePoint;
    private bool newScene = true;
    public VolumeProfile hubProfile;
    public VolumeProfile earthProfile;
    public VolumeProfile moonProfile;
    public AudioSource teleportSound;
    public AudioSource hubBGSound;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Hub")
        {
            hubProfile.TryGet<Vignette>(out vig);
            hubProfile.TryGet<FilmGrain>(out filmGrain);
            hubProfile.TryGet<ChromaticAberration>(out ca);
            filmGrain.intensity.value = 0;
            ca.intensity.value = 0;
            vig.intensity.value = 0;
            vig.center.value = new Vector2(0.5f, 0.5f);
            hubBGSound.Play();

        }
        if (SceneManager.GetActiveScene().name == "earthScene" || SceneManager.GetActiveScene().name == "BridgePractice")
        {
            earthProfile.TryGet<Vignette>(out vig);
            earthProfile.TryGet<FilmGrain>(out filmGrain);
            earthProfile.TryGet<ChromaticAberration>(out ca);
            filmGrain.intensity.value = 1;
            ca.intensity.value = 1;
            vig.intensity.value = 1;
            vig.center.value = new Vector2(0.5f, -1.5f);
            whitePoint.SetActive(false);
        }
        if (SceneManager.GetActiveScene().name == "MoonScene")
        {
            moonProfile.TryGet<Vignette>(out vig);
            moonProfile.TryGet<FilmGrain>(out filmGrain);
            moonProfile.TryGet<ChromaticAberration>(out ca);
            filmGrain.intensity.value = 1;
            ca.intensity.value = 1;
            vig.intensity.value = 1;
            vig.center.value = new Vector2(0.5f, -1.5f);
            whitePoint.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "earthScene")
        {
            if (dialogScreen.GetComponent<EarthDialogSystem>().earthThisNumDialogIsFinished == 1 && Input.GetKeyDown(KeyCode.Space))
            {
                dialogScreen.GetComponent<EarthDialogSystem>().passTheThirdActionTheJump = true;
            }
        }
        if (SceneManager.GetActiveScene().name == "MoonScene")
        {
            if (dialogScreen.GetComponent<MoonDialogSystem>().thisMoonNumDialogIsFinished == 1 && Input.GetKeyDown(KeyCode.Space))
            {
                dialogScreen.GetComponent<MoonDialogSystem>().passTheSixthActionTheJump = true;
            }
        }
        if (readyToTeleport)
        {
            if (filmGrain.intensity.value != 1)
            {
                filmGrain.intensity.value = Mathf.Lerp(filmGrain.intensity.value, 1, sizeOfEffect1);
                ca.intensity.value = Mathf.Lerp(ca.intensity.value, 1, sizeOfEffect1);
                sizeOfEffect1 = Time.deltaTime;
            }
            if (vig.intensity.value != 1 && filmGrain.intensity.value >= 0.95f)
            {
                vig.intensity.value = Mathf.Lerp(vig.intensity.value, 1, sizeOfEffect2); ;
                sizeOfEffect2 = Time.deltaTime;
            }
            if (((Vector2)vig.center).y < 1.5f && vig.intensity.value >= 0.95f)
            {
                whitePoint.SetActive(false);
                vig.center.value = new Vector2(0.5f, Mathf.Lerp(((Vector2)vig.center).y, 1.5f, sizeOfEffect3));
                sizeOfEffect3 = Time.deltaTime;
            }
            if (((Vector2)vig.center).y < 1.6f && ((Vector2)vig.center).y > 1.4f)
            {
                readyToTeleport = false;
                if (SceneManager.GetActiveScene().name == "Hub")
                {
                    SceneManager.LoadScene("earthScene");
                }
                if (SceneManager.GetActiveScene().name == "earthScene")
                {
                    SceneManager.LoadScene("MoonScene");
                }
            }
        }
        if (SceneManager.GetActiveScene().name != "Hub")
        {
            if (newScene)
            {
                if (vig.intensity.value < 0.05f)
                {
                    filmGrain.intensity.value = Mathf.Lerp(filmGrain.intensity.value, 0, sizeOfEffect1);
                    ca.intensity.value = Mathf.Lerp(ca.intensity.value, 0, sizeOfEffect1);
                    sizeOfEffect1 = Time.deltaTime;
                }
                if (((Vector2)vig.center).y > 0.45f)
                {
                    whitePoint.SetActive(true);
                    vig.intensity.value = Mathf.Lerp(vig.intensity.value, 0, sizeOfEffect2); ;
                    sizeOfEffect2 = Time.deltaTime;
                }
                if (((Vector2)vig.center).y != 0.5f)
                {
                    vig.center.value = new Vector2(0.5f, Mathf.Lerp(((Vector2)vig.center).y, 0.5f, sizeOfEffect3));
                    sizeOfEffect3 = Time.deltaTime;
                }
                if (filmGrain.intensity.value < 0.05f)
                {
                    newScene = false;
                    filmGrain.intensity.value = 0;
                    ca.intensity.value = 0;
                    vig.intensity.value = 0;
                }
            }
        }
    }
    
    public string[] GetTimelyLines()
    {
        return timelyText;
    }
    public void StartAction()
    {
        if (SceneManager.GetActiveScene().name == "Hub")
        {
            if (dialogScreen.GetComponent<DialogSystem>().thisNumDialogIsFinished == 1)
            {
                StartFirstLessonFirstAction();
            }

            if (dialogScreen.GetComponent<DialogSystem>().thisNumDialogIsFinished == 2)
            {
                StartFirstLessonSecondAction();
            }
        }

        if (SceneManager.GetActiveScene().name == "earthScene")
        {
            if (dialogScreen.GetComponent<EarthDialogSystem>().earthThisNumDialogIsFinished == 1)
            {
                StartFirstLessonThirdAction();
            }

            if (dialogScreen.GetComponent<EarthDialogSystem>().earthThisNumDialogIsFinished == 2)
            {
                StartFirstLessonFourthAction();
            }
            
            if (dialogScreen.GetComponent<EarthDialogSystem>().earthThisNumDialogIsFinished == 3)
            {
                StartFirstLessonFifthAction();
            }
        }

        if (SceneManager.GetActiveScene().name == "MoonScene")
        {
            if (dialogScreen.GetComponent<MoonDialogSystem>().thisMoonNumDialogIsFinished == 1)
            {
                StartFirstLessonSixthAction();
            }
            if (dialogScreen.GetComponent<MoonDialogSystem>().thisMoonNumDialogIsFinished == 2)
            {
                StartFirstLessonSeventhAction();
            }
            if (dialogScreen.GetComponent<MoonDialogSystem>().thisMoonNumDialogIsFinished == 3)
            {
                StartFirstLessonEigthAction();
            }
        }

        if (SceneManager.GetActiveScene().name == "BridgePractice")
        {
            if (dialogScreen.GetComponent<BridgePractice>().bridgePracticeThisNumDialogIsFinished == 1)
            {
                StartFirstLessonNinethAction();
            }
            if (dialogScreen.GetComponent<BridgePractice>().passThePractice)
            {
                StartFirstLessonTenthAction();
            }
            if (dialogScreen.GetComponent<BridgePractice>().dontPassThePractice)
            {
                StartFirstLessonEleventhAction();
            }
        }
    }
    private void StartFirstLessonFirstAction()
    {
        count = 1;
        timelyText = new string[count];
        timelyText[0] = "�������� �� �����!";
        dialogScreen.GetComponent<DialogSystem>().SetLines();
    }
    private void StartFirstLessonSecondAction()
    {
        readyToTeleport = true;
        teleportSound.Play();
    }
    private void StartFirstLessonThirdAction()
    { 
        count = 1;
        timelyText = new string[count];
        timelyText[0] = "������� ���� ������� - ��������� � ����� ������!";
        dialogScreen.GetComponent<EarthDialogSystem>().SetLines();
    }
    private void StartFirstLessonFourthAction()
    {
        count = 3;
        timelyText = new string[count];
        timelyText[0] = "�������! �������� ������� ����� ��������� ����, ������� ��������� �� ������. ������ ����� 200 ����� ��� �� 0,2 ��, ��������������, �� ���� ��������� ���� ������ F = 0,2 * 9,81 = 1,962 ������� (�). ����� ����������� ���� � ���� � ����� F = ���� ����� � �� * 9,81, ��� ����������� ������, ��� ���� ����������� �� ������.";
        timelyText[1] = "�� ��� �����, ���� �� � ����� ��������� ��������� ��� �� �������� �� ����?";
        timelyText[2] = "����� ���������! �������� �� ����!";
        dialogScreen.GetComponent<EarthDialogSystem>().SetLines();
    }
    private void StartFirstLessonFifthAction()
    {
        readyToTeleport = true;
    }
    private void StartFirstLessonSixthAction()
    {
        newScene = true;
        count = 1;
        timelyText = new string[count];
        timelyText[0] = "������� ���� ������� ���� - ����� ������, � ����� ������ ���������!";
        dialogScreen.GetComponent<MoonDialogSystem>().SetLines();
    }
    private void StartFirstLessonSeventhAction()
    {
        count = 2;
        timelyText = new string[count];
        timelyText[0] = "�������! ��� ������, ����, ������� ����������� ��� ���� � ����, ������, ��� �� �����, ��� �������� � ���, ��� ����� ���� ����������� ������ ����� �����, g ���� ���������� 16,6% �� g ����� ��� �� g ���� = 9,81 * 0,166 = 1,63 ������ �� ������� � ��������.";
        timelyText[1] = "�����, �� ����� ������ ��������� ��������� �� ���� ���� �������, �����!";
        dialogScreen.GetComponent<MoonDialogSystem>().SetLines();
    }
    private void StartFirstLessonEigthAction()
    {
        SceneManager.LoadScene("BridgePractice");
    }
    private void StartFirstLessonNinethAction()
    {
        count = 1;
        timelyText = new string[count];
        timelyText[0] = "������� � ������ ���, ����� ����� �� ����, ���� �� ��������, � ���� �������";
        dialogScreen.GetComponent<BridgePractice>().SetLines();
    }
    private void StartFirstLessonTenthAction()
    {
        count = 1;
        timelyText = new string[count];
        timelyText[0] = "��! ��� ����� �����! �������! �������!";
        dialogScreen.GetComponent<BridgePractice>().SetLines();
    }
    private void StartFirstLessonEleventhAction()
    {
        count = 1;
        timelyText = new string[count];
        timelyText[0] = "H��! T���� ����� �� �������!";
        dialogScreen.GetComponent<BridgePractice>().SetLines();
    }
}
